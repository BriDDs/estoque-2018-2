package estoque;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;;

public class SeleniumConfig {

	@Test
	public void EstoqueTest() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/100901967/Desktop/teste/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(
				"C:/Users/100901967/Desktop/teste/workspace/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");

		Select select = new Select(driver.findElement(By.id("produto")));

		select.selectByVisibleText("Manga");
		WebElement inputQuantidade = driver.findElement(By.id("quantidade"));
		inputQuantidade.sendKeys("10");
		WebElement inputValorUnitario = driver.findElement(By.id("valorUnitario"));
		inputValorUnitario.sendKeys("5");

		WebElement buttonCalcular = driver.findElement(By.id("calcularBtn"));
		buttonCalcular.click();

		WebElement output = driver.findElement(By.id("valortotal"));
		int esperado = 50;
		assertEquals(esperado, output);
	}

}
